package br.com.santander.internet_banking.controller;

import br.com.santander.internet_banking.controller.assembler.AccountAssembler;
import br.com.santander.internet_banking.controller.assembler.TransactionAssembler;
import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.dto.AccountDTO;
import br.com.santander.internet_banking.service.AccountService;
import br.com.santander.internet_banking.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private AccountService accountService;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private AccountAssembler accountAssembler;

    @MockBean
    private TransactionAssembler transactionAssembler;

    @Test
    public void retrieveAccountTest() throws Exception {
        Account account = new Account("João da Silva", LocalDate.of(2021, 5, 1), true);
        given(accountService.get(1L)).willReturn(account);
        given(accountAssembler.toModel(account)).willReturn(EntityModel.of(account));

        ResponseEntity<Account> accountResponse = restTemplate.getForEntity("/banking/v1/accounts/1", Account.class);

        assertThat(accountResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(accountResponse.getBody()).isNotNull();
        assertThat(accountResponse.getBody().toString()).isEqualTo(account.toString());
    }

    @Test
    public void createAccountTest() throws Exception {
        AccountDTO accountDTO = new AccountDTO("João da Silva", LocalDate.of(2021, 5, 1), true);
        Account account = new Account(accountDTO);

        EntityModel<Account> accountEntityModel = EntityModel.of(account);
        accountEntityModel.add(Link.of("/banking/v1/accounts/1").withSelfRel());

        given(accountService.createOrUpdate(accountDTO)).willReturn(account);
        given(accountAssembler.toModel(account)).willReturn(accountEntityModel);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AccountDTO> request = new HttpEntity<AccountDTO>(accountDTO, headers);
        ResponseEntity<Account> accountResponse = restTemplate.postForEntity("/banking/v1/accounts/", request, Account.class);

        assertThat(accountResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(accountResponse.getBody()).isNotNull();
        assertThat(accountResponse.getBody().getClient()).isEqualTo(accountDTO.getClient());
    }

    @Test
    public void retrieveAllAccountTest() throws Exception {
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account("João da Silva", LocalDate.of(2021, 5, 1), true));
        accounts.add(new Account("Maria da Silva", LocalDate.of(2021, 4, 15), false));
        accounts.add(new Account("Carlos da Silva", LocalDate.of(2021, 5, 23), true));

        Page<Account> accountsPage = new PageImpl<>(accounts);
        List<EntityModel<Account>> accountEntities = accountsPage.stream().map(EntityModel::of).collect(Collectors.toList());
        CollectionModel<EntityModel<Account>> entityModel = PagedModel.of(accountEntities);

        given(accountService.getAll(PageRequest.of(0, 20))).willReturn(accountsPage);
        given(accountAssembler.toPagedModel(accountsPage)).willReturn(entityModel);

        ResponseEntity<Account> accountResponse = restTemplate.getForEntity("/banking/v1/accounts/", Account.class);

        assertThat(accountResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(accountResponse.getBody()).isNotNull();
    }
}