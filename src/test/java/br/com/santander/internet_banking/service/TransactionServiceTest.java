package br.com.santander.internet_banking.service;

import br.com.santander.internet_banking.exceptions.NotEnoughFundsException;
import br.com.santander.internet_banking.exceptions.TransactionNotAllowedException;
import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.Transaction;
import br.com.santander.internet_banking.model.TransactionType;
import br.com.santander.internet_banking.model.dto.TransactionDTO;
import br.com.santander.internet_banking.model.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    void processDepositTransaction() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.DEPOSIT, 200.0);

        when(transactionRepository.save(isA(Transaction.class))).thenReturn(isA(Transaction.class));
        transactionService.process(account, transactionDTO);

        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(200.0));
    }

    @Test
    void processWithDrawTransactionExemptFee() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        account.setBalance(BigDecimal.valueOf(1000.0));
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.WITHDRAW, 100.0);

        when(transactionRepository.save(isA(Transaction.class))).thenReturn(isA(Transaction.class));
        transactionService.process(account, transactionDTO);

        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(900.0));
    }

    @Test
    void processWithDrawTransaction0004Fee() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        account.setBalance(BigDecimal.valueOf(1000.0));
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.WITHDRAW, 200.0);

        when(transactionRepository.save(isA(Transaction.class))).thenReturn(isA(Transaction.class));
        transactionService.process(account, transactionDTO);

        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(799.2));
    }

    @Test
    void processWithDrawTransaction001Fee() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        account.setBalance(BigDecimal.valueOf(1000.0));
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.WITHDRAW, 400.0);

        when(transactionRepository.save(isA(Transaction.class))).thenReturn(isA(Transaction.class));
        transactionService.process(account, transactionDTO);

        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(596.0));
    }

    @Test
    void processWithDrawTransaction001FeeExclusivePlan() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), true);
        account.setBalance(BigDecimal.valueOf(1000.0));
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.WITHDRAW, 400.0);

        when(transactionRepository.save(isA(Transaction.class))).thenReturn(isA(Transaction.class));
        transactionService.process(account, transactionDTO);

        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(600.0));
    }

    @Test
    void processFeeTransactionNotAllowed() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.FEE, 200.0);

        assertThatThrownBy(() -> transactionService.process(account, transactionDTO))
                .isInstanceOf(TransactionNotAllowedException.class)
                .hasMessageContaining("FEE");
    }

    @Test
    void processWithdrawTransactionNotEnoughFunds() {
        Account account = new Account("Dummy", LocalDate.of(2021, 5, 15), false);
        TransactionDTO transactionDTO = new TransactionDTO(TransactionType.WITHDRAW, 200.0);

        assertThatThrownBy(() -> transactionService.process(account, transactionDTO))
                .isInstanceOf(NotEnoughFundsException.class)
                .hasMessageContaining("Not enough funds");
    }
}