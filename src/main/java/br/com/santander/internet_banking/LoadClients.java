package br.com.santander.internet_banking;

import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
public class LoadClients {

    private static final Logger log = LoggerFactory.getLogger(LoadClients.class);

    @Bean
    CommandLineRunner startDatabase(AccountRepository accountRepository) {
        return args -> {
            log.info("Carregando cliente: {}", accountRepository
                    .save(new Account("João da Silva",
                            LocalDate.of(1992, 2, 15), false)));
            log.info("Carregando cliente: {}", accountRepository
                    .save(new Account("Maria da Silva",
                            LocalDate.of(1984, 1, 28), true)));
            log.info("Carregando cliente: {}", accountRepository
                    .save(new Account("Carlos da Silva",
                            LocalDate.of(1945, 6, 1), true)));
        };
    }
}
