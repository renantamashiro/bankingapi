package br.com.santander.internet_banking.service;

import br.com.santander.internet_banking.exceptions.AccountNotFoundException;
import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.dto.AccountDTO;
import br.com.santander.internet_banking.model.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @CacheEvict(cacheNames = "accounts", allEntries = true)
    public Account createOrUpdate(AccountDTO accountDTO) {
        return this.accountRepository.save(new Account(accountDTO));
    }

    @CacheEvict(cacheNames = "accounts", allEntries = true)
    public Account createOrUpdate(Account account) {
        return this.accountRepository.save(account);
    }

    @Cacheable(cacheNames = "accounts", key = "#paging.getPageNumber()")
    public Page<Account> getAll(Pageable paging) {
        return this.accountRepository.findAll(paging);
    }

    @Cacheable(cacheNames = "accounts", key = "#id")
    public Account get(Long id) {
        return this.accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));
    }
}
