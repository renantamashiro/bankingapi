package br.com.santander.internet_banking.service;

import br.com.santander.internet_banking.exceptions.TransactionNotAllowedException;
import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.Transaction;
import br.com.santander.internet_banking.model.TransactionType;
import br.com.santander.internet_banking.model.dto.TransactionDTO;
import br.com.santander.internet_banking.model.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void process(Account account, TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction(transactionDTO.getType(), transactionDTO.getValue());
        TransactionType transactionType = transactionDTO.getType();

        if (transactionType.equals(TransactionType.DEPOSIT)) {
            account.deposit(transaction);

        } else if (transactionType.equals(TransactionType.WITHDRAW)) {
            double adminFee = account.withdraw(transaction);
            Transaction feeTransaction = new Transaction(TransactionType.FEE, adminFee);

            if (adminFee > 0) {
                feeTransaction.setAccount(account);
                feeTransaction.setDateTime(LocalDateTime.now());
                this.transactionRepository.save(feeTransaction);
            }
        } else {
            throw new TransactionNotAllowedException(transactionType);
        }

        transaction.setAccount(account);
        transaction.setDateTime(LocalDateTime.now());
        this.transactionRepository.save(transaction);
    }

    public Page<Transaction> getAll(Pageable paging) {
        return this.transactionRepository.findAll(paging);
    }

    public Page<Transaction> getAllBy(Account account, Pageable paging) {
        return this.transactionRepository.findAllByAccount(account, paging);
    }

    public Page<Transaction> getAllBy(Account account, LocalDate date, Pageable paging) {
        return this.transactionRepository.findAllByAccountAndDate(account, date, paging);
    }

    public Page<Transaction> getAllBy(Account account, LocalDate date, TransactionType transactionType, Pageable paging) {
        return this.transactionRepository.findAllByAccountAndDateAndType(account, date, transactionType, paging);
    }

    public Page<Transaction> getAllBy(LocalDate date, Pageable paging) {
        return this.transactionRepository.findAllByDate(date, paging);
    }

    public Page<Transaction> getAllBy(LocalDate date, TransactionType transactionType, Pageable paging) {
        return this.transactionRepository.findAllByDateAndType(date, transactionType, paging);
    }
}
