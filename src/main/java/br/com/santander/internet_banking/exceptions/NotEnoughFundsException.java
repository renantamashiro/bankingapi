package br.com.santander.internet_banking.exceptions;

import java.math.BigDecimal;

public class NotEnoughFundsException extends RuntimeException {

    public NotEnoughFundsException(BigDecimal balance, BigDecimal transactionValue) {
        super("Not enough funds for withdraw. Necessary funds: " + transactionValue + ", current balance: " + balance + "\n");
    }
}
