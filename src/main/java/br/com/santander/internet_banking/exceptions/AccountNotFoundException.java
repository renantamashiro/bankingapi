package br.com.santander.internet_banking.exceptions;

public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(Long id) {
        super("Could not find client " + id + "\n");
    }
}
