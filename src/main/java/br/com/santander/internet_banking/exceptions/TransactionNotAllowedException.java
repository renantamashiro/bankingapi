package br.com.santander.internet_banking.exceptions;

import br.com.santander.internet_banking.model.TransactionType;

public class TransactionNotAllowedException extends RuntimeException {

    public TransactionNotAllowedException(TransactionType transactionType) {
        super("This type of transaction is not allowed: " + transactionType + "\n");
    }
}
