package br.com.santander.internet_banking.controller;

import br.com.santander.internet_banking.controller.assembler.AccountAssembler;
import br.com.santander.internet_banking.controller.assembler.TransactionAssembler;
import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.Transaction;
import br.com.santander.internet_banking.model.TransactionType;
import br.com.santander.internet_banking.model.dto.AccountDTO;
import br.com.santander.internet_banking.model.dto.TransactionDTO;
import br.com.santander.internet_banking.service.AccountService;
import br.com.santander.internet_banking.service.TransactionService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping(value = "/banking/v1/accounts")
@OpenAPIDefinition(info = @Info(title = "Account Management",
        description = "Manage accounts and make transactions per account"))
public class AccountController {

    private final AccountService accountService;
    private final AccountAssembler accountAssembler;

    private final TransactionAssembler transactionAssembler;
    private final TransactionService transactionService;

    public AccountController(AccountService accountService,
                             AccountAssembler accountAssembler,
                             TransactionAssembler transactionAssembler,
                             TransactionService transactionService) {
        this.accountService = accountService;
        this.accountAssembler = accountAssembler;
        this.transactionAssembler = transactionAssembler;
        this.transactionService = transactionService;
    }

    @PostMapping("")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account created", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Account.class))
                    }),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)},
            tags = "Accounts"
    )
    public ResponseEntity<?> newAccount(@RequestBody AccountDTO accountDTO) {
        EntityModel<Account> accountEntityModel = this.accountAssembler
                .toModel(this.accountService.createOrUpdate(accountDTO));

        return ResponseEntity
                .created(accountEntityModel
                        .getRequiredLink(IanaLinkRelations.SELF)
                        .toUri())
                .body(accountEntityModel);
    }

    @GetMapping("")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "200", description = "Accounts found", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Account.class))
                    })
            },
            tags = "Accounts"
    )
    public ResponseEntity<?> all(@ParameterObject Pageable pageable) {
        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.accountAssembler.toPagedModel(this.accountService.getAll(pageable)));
    }

    @GetMapping("/{id}")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "200", description = "Accounts found", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Account.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Invalid Id Account", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)
            }, tags = "Accounts"
    )
    public ResponseEntity<?> oneAccount(@Parameter(description = "Account ID to be searched") @PathVariable Long id) {
        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.accountAssembler.toModel(this.accountService.get(id)));
    }

    @PutMapping("/{id}/transactions")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Transaction processed", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Account.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Invalid Id Account", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)
            }, tags = "Account Transactions"
    )
    public ResponseEntity<?> makeTransaction(
            @Parameter(description = "Account ID to be searched") @PathVariable Long id,
            @RequestBody() TransactionDTO transactionDTO) {

        Account account = this.accountService.get(id);
        this.transactionService.process(account, transactionDTO);

        EntityModel<Account> accountEntityModel = this.accountAssembler
                .toModel(this.accountService.createOrUpdate(account));

        return ResponseEntity
                .created(accountEntityModel
                        .getRequiredLink(IanaLinkRelations.SELF)
                        .toUri())
                .body(accountEntityModel);
    }

    @GetMapping("/{id}/transactions")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account transactions", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Invalid Id Account", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)
            }, tags = "Account Transactions"
    )
    public ResponseEntity<?> getTransactionsByAccount(
            @Parameter(description = "Account ID to be searched") @PathVariable Long id,
            @ParameterObject Pageable pageable) {

        Account account = this.accountService.get(id);
        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(this.transactionService.getAllBy(account, pageable), id));
    }

    @GetMapping("/{id}/transactions/{date}")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account transactions by date", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Invalid Id Account", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)
            }, tags = "Account Transactions"
    )
    public ResponseEntity<?> allTransactionsByDateAndAccount(
            @Parameter(description = "Account ID to be searched") @PathVariable Long id,
            @Parameter(description = "Date to be searched", schema = @Schema(example = "31-12-1996")) @PathVariable String date,
            @ParameterObject Pageable pageable) {

        Account account = this.accountService.get(id);
        LocalDate transactionDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        Page<Transaction> transactionsPaged = this.transactionService.getAllBy(account, transactionDate, pageable);

        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(transactionsPaged, id));
    }

    @GetMapping("/{id}/transactions/{date}/{type}")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Account transactions by date and transaction type", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Invalid Id Account", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Account not found", content = @Content)
            }, tags = "Account Transactions"
    )
    public ResponseEntity<?> allTransactionsByDateTypeAndAccount(
            @Parameter(description = "Account ID to be searched") @PathVariable Long id,
            @Parameter(description = "Date to be searched", schema = @Schema(example = "31-12-1996")) @PathVariable String date,
            @Parameter(description = "Transaction type to be searched", schema = @Schema(example = "FEE")) @PathVariable String type,
            @ParameterObject Pageable pageable) {

        Account account = this.accountService.get(id);
        LocalDate transactionDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        Page<Transaction> transactionsPaged = this.transactionService
                .getAllBy(account, transactionDate, TransactionType.valueOf(type.toUpperCase()), pageable);

        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(transactionsPaged, id));
    }

    @GetMapping("/transactions")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Transactions of all accounts", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    })
            }, tags = "Transactions"
    )
    public ResponseEntity<?> allTransactions(@ParameterObject Pageable pageable) {
        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(this.transactionService.getAll(pageable)));
    }

    @GetMapping("/transactions/{date}")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Transactions of all accounts by date", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    })
            }, tags = "Transactions"
    )
    public ResponseEntity<?> allTransactionsByDate(
            @Parameter(description = "Date to be searched", schema = @Schema(example = "31-12-1996")) @PathVariable String date,
            @ParameterObject Pageable pageable) {

        LocalDate transactionDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        Page<Transaction> transactionsPaged = this.transactionService.getAllBy(transactionDate, pageable);

        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(transactionsPaged));
    }

    @GetMapping("/transactions/{date}/{type}")
    @Operation(
            responses = {
                    @ApiResponse(responseCode = "201", description = "Transactions of all accounts by date and transaction type", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Transaction.class))
                    })
            }, tags = "Transactions"
    )
    public ResponseEntity<?> allTransactionsByDateAndType(
            @Parameter(description = "Date to be searched", schema = @Schema(example = "31-12-1996")) @PathVariable String date,
            @Parameter(description = "Transaction type to be searched", schema = @Schema(example = "FEE")) @PathVariable String type,
            @ParameterObject Pageable pageable) {

        LocalDate transactionDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        Page<Transaction> transactionsPaged = this.transactionService
                .getAllBy(transactionDate, TransactionType.valueOf(type.toUpperCase()), pageable);

        return ResponseEntity
                .ok()
                .contentType(MediaTypes.HAL_JSON)
                .body(this.transactionAssembler.toPagedModel(transactionsPaged));
    }
}
