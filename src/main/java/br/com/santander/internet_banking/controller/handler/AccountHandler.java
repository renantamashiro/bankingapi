package br.com.santander.internet_banking.controller.handler;

import br.com.santander.internet_banking.exceptions.AccountNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class AccountHandler {

    @ResponseBody
    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ResponseEntity<String> accountNotFoundHandler(AccountNotFoundException anfe) {
        return new ResponseEntity<>(anfe.getMessage(), HttpStatus.NOT_FOUND);
    }
}
