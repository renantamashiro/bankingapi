package br.com.santander.internet_banking.controller.assembler;

import br.com.santander.internet_banking.controller.AccountController;
import br.com.santander.internet_banking.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TransactionAssembler implements RepresentationModelAssembler<Transaction, EntityModel<Transaction>> {

    @Autowired
    private PagedResourcesAssembler<Transaction> pagedResourcesAssembler;

    @Override
    public EntityModel<Transaction> toModel(Transaction transaction) {

        return EntityModel.of(transaction,
                linkTo(methodOn(AccountController.class)
                        .oneAccount(transaction.getAccount().getId())).withRel("account"),
                linkTo(methodOn(AccountController.class)
                        .getTransactionsByAccount(transaction.getAccount().getId(), PageRequest.of(0, 20)))
                        .withRel("transactions"));
    }

    public CollectionModel<EntityModel<Transaction>> toPagedModel(Page<Transaction> transactions) {
        PagedModel<EntityModel<Transaction>> transactionsPaged = this.pagedResourcesAssembler
                .toModel(transactions, this);
        transactionsPaged.add(linkTo(methodOn(AccountController.class)
                .all(PageRequest.of(0, 20))).withRel("accounts"));

        return transactionsPaged;
    }

    public CollectionModel<EntityModel<Transaction>> toPagedModel(Page<Transaction> transactions, Long accountId) {
        PagedModel<EntityModel<Transaction>> transactionsPaged = this.pagedResourcesAssembler
                .toModel(transactions, this);

        transactionsPaged
                .add(linkTo(methodOn(AccountController.class)
                        .oneAccount(accountId)).withRel("account"))
                .add(linkTo(methodOn(AccountController.class)
                        .all(PageRequest.of(0, 20))).withRel("accounts"));

        return transactionsPaged;
    }
}
