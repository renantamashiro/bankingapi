package br.com.santander.internet_banking.controller.handler;

import br.com.santander.internet_banking.exceptions.NotEnoughFundsException;
import br.com.santander.internet_banking.exceptions.TransactionNotAllowedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class TransactionHandler {

    @ResponseBody
    @ExceptionHandler(TransactionNotAllowedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> transactionNotAllowedHandler(TransactionNotAllowedException tnae) {
        return new ResponseEntity<>(tnae.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(NotEnoughFundsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> notEnoughFundsHandler(NotEnoughFundsException nefe) {
        return new ResponseEntity<>(nefe.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
