package br.com.santander.internet_banking.controller.assembler;

import br.com.santander.internet_banking.controller.AccountController;
import br.com.santander.internet_banking.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AccountAssembler implements RepresentationModelAssembler<Account, EntityModel<Account>> {

    @Autowired
    private PagedResourcesAssembler<Account> pagedResourcesAssembler;

    @Override
    public EntityModel<Account> toModel(Account account) {

        return EntityModel.of(account,
                linkTo(methodOn(AccountController.class).oneAccount(account.getId())).withSelfRel(),
                linkTo(methodOn(AccountController.class)
                        .getTransactionsByAccount(account.getId(), PageRequest.of(0, 20)))
                        .withRel("transactions"),
                linkTo(methodOn(AccountController.class).all(PageRequest.of(0, 20))).withRel("accounts"));
    }

    public CollectionModel<EntityModel<Account>> toPagedModel(Page<Account> accounts) {
        PagedModel<EntityModel<Account>> accountsPaged = this.pagedResourcesAssembler.toModel(accounts, this);
        accountsPaged.add(linkTo(methodOn(AccountController.class)
                .allTransactions(PageRequest.of(0, 20))).withRel("transactions"));

        return accountsPaged;
    }
}
