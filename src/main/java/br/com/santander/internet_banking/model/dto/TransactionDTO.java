package br.com.santander.internet_banking.model.dto;


import br.com.santander.internet_banking.model.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TransactionDTO {
    private TransactionType type;
    private Double value;
}
