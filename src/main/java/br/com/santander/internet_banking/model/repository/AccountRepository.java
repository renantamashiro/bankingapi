package br.com.santander.internet_banking.model.repository;

import br.com.santander.internet_banking.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AccountRepository extends JpaRepository<Account, Long> {

}
