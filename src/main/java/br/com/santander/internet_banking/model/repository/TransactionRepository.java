package br.com.santander.internet_banking.model.repository;

import br.com.santander.internet_banking.model.Account;
import br.com.santander.internet_banking.model.Transaction;
import br.com.santander.internet_banking.model.TransactionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Page<Transaction> findAllByAccount(Account account, Pageable paging);

    Page<Transaction> findAllByDate(LocalDate date, Pageable paging);

    Page<Transaction> findAllByDateAndType(LocalDate date, TransactionType transactionType, Pageable paging);

    Page<Transaction> findAllByAccountAndDateAndType(Account account, LocalDate date, TransactionType transactionType, Pageable paging);

    Page<Transaction> findAllByAccountAndDate(Account account, LocalDate date, Pageable paging);
}
