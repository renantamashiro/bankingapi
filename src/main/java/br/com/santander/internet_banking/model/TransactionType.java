package br.com.santander.internet_banking.model;

public enum TransactionType {
    WITHDRAW,
    DEPOSIT,
    FEE
}
