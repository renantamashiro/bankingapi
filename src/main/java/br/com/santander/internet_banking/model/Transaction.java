package br.com.santander.internet_banking.model;

import br.com.santander.internet_banking.model.dto.TransactionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue
    @Column(name = "tr_id")
    private Long id;

    @Column(name = "tr_type")
    private TransactionType type;

    @Column(name = "tr_date")
    private LocalDate date;

    @Column(name = "tr_time")
    private LocalTime time;

    @Column(name = "tr_value")
    private Double value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk01_account")
    private Account account;

    public Transaction(TransactionType type, Double value) {
        this.type = type;
        this.value = value;
    }

    public Transaction(TransactionDTO transactionDTO) {
        this.type = transactionDTO.getType();
        this.value = transactionDTO.getValue();
    }

    public void setDateTime(LocalDateTime localDateTime) {
        this.date = localDateTime.toLocalDate();
        this.time = localDateTime.toLocalTime();
    }

}
