package br.com.santander.internet_banking.model;

import br.com.santander.internet_banking.exceptions.NotEnoughFundsException;
import br.com.santander.internet_banking.model.dto.AccountDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accounts_seq")
    @SequenceGenerator(name = "accounts_seq", sequenceName = "accounts_seq", allocationSize = 1)
    private Long id;

    @Column(name = "client")
    private String client;
    @Column(name = "birthdate")
    private LocalDate birthdate;
    @Column(name = "exclusive_plan")
    private Boolean exclusivePlan;
    @Column(name = "balance")
    private BigDecimal balance = new BigDecimal(0);

    public Account() {

    }

    public Account(String client, LocalDate birthdate, Boolean exclusivePlan) {
        this.client = client;
        this.birthdate = birthdate;
        this.exclusivePlan = exclusivePlan;
    }

    public Account(AccountDTO accountDTO) {
        this.client = accountDTO.getClient();
        this.birthdate = accountDTO.getBirthdate();
        this.exclusivePlan = accountDTO.getExclusivePlan();
    }

    public double withdraw(Transaction transaction) {
        if (transaction.getType().equals(TransactionType.WITHDRAW)) {
            double fee = this.calculateFee(transaction);
            if (this.exclusivePlan)
                fee = 0.;

            BigDecimal transactionValue = BigDecimal.valueOf(transaction.getValue() + fee);
            if (transactionValue.compareTo(this.balance) > 0) {
                throw new NotEnoughFundsException(this.balance, transactionValue);
            }

            this.setBalance(this.balance.subtract(transactionValue));

            return fee;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void deposit(Transaction transaction) {
        if (transaction.getType().equals(TransactionType.DEPOSIT)) {
            this.setBalance(this.balance.add(BigDecimal.valueOf(transaction.getValue())));
        } else {
            throw new IllegalArgumentException();
        }
    }

    private double calculateFee(Transaction transaction) {
        double transactionValue = transaction.getValue();
        double fee = 0.;

        if (transactionValue > 300) {
            fee = 0.01;
        } else if (transactionValue > 100) {
            fee = 0.004;
        }

        return transactionValue * fee;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + this.id +
                ", name='" + this.client + '\'' + '}';
    }
}
