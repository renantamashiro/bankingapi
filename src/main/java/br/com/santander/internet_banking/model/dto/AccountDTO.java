package br.com.santander.internet_banking.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class AccountDTO {

    private String client;
    private LocalDate birthdate;
    private Boolean exclusivePlan;

}
