drop table if exists accounts;

create sequence accounts_seq minvalue 1 maxvalue 100000000 start with 1 increment by 1;

create table accounts (
    id int default accounts_seq.nextval primary key,
    client varchar(250) not null,
    birthdate date not null,
    exclusive_plan varchar(10),
    balance double not null
);

drop table if exists transactions;

create table transactions (
    tr_id int auto_increment primary key,
    tr_type varchar(250) not null,
    tr_date date not null,
    tr_time time not null,
    tr_value varchar(10),
    fk01_account int not null
);

alter table transactions
add constraint fk01_account foreign key(fk01_account)
references accounts(id);
